use std::fmt::Display;

use chrono::{DateTime, Duration, FixedOffset, Local, NaiveDateTime};
use log::{debug, error, warn};
use serde::Deserialize;
use thiserror::Error;
use uom::si::{
    f32::ThermodynamicTemperature as Temperature,
    thermodynamic_temperature::{degree_celsius, degree_fahrenheit},
};

use super::config::Config;

#[derive(Deserialize, Debug)]
struct Response {
    utc_offset_seconds: i32,
    hourly_units: HourlyUnits,
    hourly: Hourly,
}
#[derive(Deserialize, Debug, Clone, Copy)]
enum TimeFormat {
    #[serde(rename = "unixtime")]
    Unixtime,
    #[serde(rename = "iso8601")]
    Iso8601,
}
impl Display for TimeFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Unixtime => write!(f, "unix timestamp"),
            Self::Iso8601 => write!(f, "iso8601"),
        }
    }
}
#[derive(Debug, Error)]
enum TimeFormatError {
    #[error("{0} is no i64")]
    NoI64(serde_json::Number),
    #[error("{0} is no timestamp")]
    NoTimestamp(i64),
    #[error("Cannot understand {1} when time format is {0}")]
    IncompatibleTimeFormatAndJsonValue(String, serde_json::Value),
    #[error(transparent)]
    ParseError(chrono::ParseError),
}
impl TimeFormat {
    fn parse_json(&self, v: serde_json::Value) -> Result<NaiveDateTime, TimeFormatError> {
        match v {
            serde_json::Value::Number(ref n) => match self {
                Self::Unixtime => {
                    let ts = n.as_i64().ok_or(TimeFormatError::NoI64(n.clone()))?;
                    NaiveDateTime::from_timestamp_opt(ts, 0).ok_or(TimeFormatError::NoTimestamp(ts))
                }
                Self::Iso8601 => Err(TimeFormatError::IncompatibleTimeFormatAndJsonValue(
                    self.to_string(),
                    v,
                )),
            },
            serde_json::Value::String(ref s) => match self {
                Self::Unixtime => Err(TimeFormatError::IncompatibleTimeFormatAndJsonValue(
                    self.to_string(),
                    v,
                )),
                Self::Iso8601 => NaiveDateTime::parse_from_str(s, "%Y-%m-%dT%H:%M")
                    .map_err(TimeFormatError::ParseError),
            },
            serde_json::Value::Null
            | serde_json::Value::Bool(_)
            | serde_json::Value::Array(_)
            | serde_json::Value::Object(_) => Err(
                TimeFormatError::IncompatibleTimeFormatAndJsonValue(self.to_string(), v),
            ),
        }
    }
}
#[derive(Deserialize, Debug)]
enum TemperatureFormat {
    #[serde(rename = "°C")]
    Celcius,
    #[serde(rename = "°F")]
    Fahrenheit,
}
impl TemperatureFormat {
    fn parse_f32(&self, t: f32) -> Temperature {
        match self {
            Self::Celcius => Temperature::new::<degree_celsius>(t),
            Self::Fahrenheit => Temperature::new::<degree_fahrenheit>(t),
        }
    }
}
#[derive(Deserialize, Debug)]
struct HourlyUnits {
    time: TimeFormat,
    #[serde(rename = "temperature_2m")]
    temperature_unit: TemperatureFormat,
}
#[derive(Deserialize, Debug)]
struct Hourly {
    time: Vec<serde_json::Value>,
    temperature_2m: Vec<f32>,
}

#[derive(Debug, Error)]
enum TimeConversionError<T> {
    #[error("Could not convert {0} to tz_offset {1}")]
    None(NaiveDateTime, String),
    #[error("{0} in {1} yields possibilites ({2}, {3})")]
    Ambiguous(NaiveDateTime, String, T, T),
}

fn naive_and_tz_to_local(
    ndt: NaiveDateTime,
    tz: FixedOffset,
) -> Result<DateTime<Local>, TimeConversionError<DateTime<FixedOffset>>> {
    match ndt.and_local_timezone(tz) {
        chrono::LocalResult::None => Err(TimeConversionError::None(ndt, tz.to_string())),
        chrono::LocalResult::Ambiguous(t1, t2) => {
            Err(TimeConversionError::Ambiguous(ndt, tz.to_string(), t1, t2))
        }
        chrono::LocalResult::Single(dt) => Ok(dt.with_timezone(&chrono::Local)),
    }
}

#[derive(Debug, Error)]
#[error("Could not compute offset from {0}")]
struct TimeZoneError(i32);

impl Response {
    fn timezone(&self) -> Result<FixedOffset, TimeZoneError> {
        FixedOffset::east_opt(self.utc_offset_seconds).ok_or(TimeZoneError(self.utc_offset_seconds))
    }

    fn temperatures(self) -> impl Iterator<Item = Option<(DateTime<Local>, Temperature)>> {
        let tz = self.timezone().map_err(|e| error!("{e}")).unwrap();
        self.hourly
            .time
            .into_iter()
            .zip(self.hourly.temperature_2m)
            .map(move |(time, temp)| {
                let ndt = self
                    .hourly_units
                    .time
                    .parse_json(time)
                    .inspect_err(|e| warn!("{e}"))
                    .ok()?;
                let dt = naive_and_tz_to_local(ndt, tz)
                    .inspect_err(|e| warn!("{e}"))
                    .ok()?;
                let temp = self.hourly_units.temperature_unit.parse_f32(temp);
                Some((dt, temp))
            })
    }
}

fn call_api(url: &str) -> Response {
    ureq::get(url)
        .call()
        .inspect_err(|e| error!("{e}"))
        .expect("Api call failed")
        .into_json::<Response>()
        .inspect_err(|e| error!("{e}"))
        .expect("Parsing the json into a response failed")
}

/// Fetchest a forecast for the temperatures of the next 3 days
pub fn temperatures(
    config: &Config,
) -> impl Iterator<Item = Option<(DateTime<Local>, Temperature)>> {
    let url = config.api_url();
    let resp = call_api(url);
    debug!("{resp:#?}");
    resp.temperatures()
}

/// Returns the number of times temperatures are <= the threshold on the next day
pub fn sub_threshold_hours<I>(temperatures: I, threshold_tmp: Temperature) -> usize
where
    I: IntoIterator<Item = (DateTime<Local>, Temperature)>,
{
    let now = Local::now();
    let sometime_tomorrow = now + Duration::days(1);
    debug!("sometime_tomorrow: {sometime_tomorrow}");

    temperatures
        .into_iter()
        .skip_while(|(t, _)| *t < now)
        .take_while(|(t, _)| t.date_naive() <= sometime_tomorrow.date_naive())
        .filter(|(_, t)| *t <= threshold_tmp)
        .count()
}
