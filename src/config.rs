use std::{
    fs::{self, File},
    io::{self, BufReader},
    path::{Path, PathBuf},
};

use log::{error, warn};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use uom::si::{
    f32::ThermodynamicTemperature as Temperature, thermodynamic_temperature::degree_celsius,
};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    zero: Temperature,
    sub_hour_min: usize,
    api_url: String,
    mailpath: PathBuf,
}
impl Config {
    pub fn zero(&self) -> Temperature {
        self.zero
    }

    pub fn sub_hour_min(&self) -> usize {
        self.sub_hour_min
    }

    pub fn api_url(&self) -> &str {
        self.api_url.as_ref()
    }

    pub fn mailpath(&self) -> &Path {
        &self.mailpath
    }

    pub fn zero_mut(&mut self) -> &mut Temperature {
        &mut self.zero
    }

    pub fn sub_hour_min_mut(&mut self) -> &mut usize {
        &mut self.sub_hour_min
    }

    pub fn api_url_mut(&mut self) -> &mut String {
        &mut self.api_url
    }

    pub fn mailpath_mut(&mut self) -> &mut Path {
        &mut self.mailpath
    }

    pub fn set_zero(&mut self, zero: Temperature) {
        self.zero = zero;
    }

    pub fn set_sub_hour_min(&mut self, sub_hour_min: usize) {
        self.sub_hour_min = sub_hour_min;
    }

    pub fn set_api_url(&mut self, api_url: &str) {
        self.api_url = api_url.to_string();
    }

    pub fn set_mailpath(&mut self, mailpath: &Path) {
        self.mailpath = mailpath.to_path_buf();
    }
}
impl Default for Config {
    fn default() -> Self {
        Config {
            zero: Temperature::new::<degree_celsius>(0.0),
            sub_hour_min: 1,
            api_url: "https://api.open-meteo.com/v1/dwd-icon?latitude=49.6228&longitude=10.8253&hourly=temperature_2m&timezone=Europe%2FBerlin&forecast_days=3".to_string(),
            mailpath: xdg::dirs::data_home()
        .join("sub-zero-notifier/mail.md")
        }
    }
}

fn read_config_from_file<P: AsRef<Path>>(path: P) -> Option<Config> {
    let path = path.as_ref();
    let file = File::open(path)
        .map_err(|e| warn!("Could not read config at {}: {e}", path.display()))
        .ok()?;
    let reader = BufReader::new(file);

    // Read the JSON contents of the file as an instance of `Config`.
    let u = serde_json::from_reader(reader)
        .map_err(|e| error!("Could not parse config at {}: {e}", path.display()))
        .ok()?;

    // Return the `Config`.
    Some(u)
}

#[derive(Debug, Error)]
pub enum SaveError {
    #[error("Could not open config at {0}: {1}")]
    Io(String, io::Error),
    #[error("Could not serialize config to string: {0}")]
    Json(serde_json::Error),
}

fn save_config_to_file<P: AsRef<Path>>(path: P, config: &Config) -> Result<(), SaveError> {
    let path = path.as_ref();

    let config_s = serde_json::to_string_pretty(&config)
        .map_err(SaveError::Json)
        .inspect_err(|e| error!("{e}"))?;

    fs::write(path, config_s)
        .map_err(|e| SaveError::Io(path.display().to_string(), e))
        .inspect_err(|e| error!("{e}"))
}

fn configpath() -> PathBuf {
    xdg::dirs::config_home().join("sub-zero-notifier/config.json")
}

pub fn load() -> Config {
    read_config_from_file(configpath()).unwrap_or_else(|| {
        let c = Config::default();
        save(&c);
        c
    })
}

pub fn save(config: &Config) -> bool {
    save_config_to_file(configpath(), config).is_ok()
}
