use log::{debug, error, info};
use uom::si::{
    f32::ThermodynamicTemperature as Temperature, thermodynamic_temperature::degree_celsius,
};

mod config;
mod weather;

fn main() {
    pretty_env_logger::init();
    let config = config::load();

    let ts = weather::temperatures(&config).flatten();

    let zero = config.zero();
    let format = Temperature::format_args(degree_celsius, uom::fmt::DisplayStyle::Abbreviation);
    let cnt = weather::sub_threshold_hours(ts, zero);
    debug!("sub_{}_hours: {cnt}", format.with(zero));

    if cnt < config.sub_hour_min() {
        info!("No hours below the threshold tomorrow");
        return;
    }

    let mailpath = config.mailpath();
    debug!("mailpath: {mailpath:?}");

    let mail = std::fs::read_to_string(mailpath).unwrap_or_else(|e| {
        error!("Could not read {}: {e}", mailpath.display());
        panic!("Could not read {}: {e}", mailpath.display())
    });

    println!("{}", mail);
}
